const axios = require("axios");
const Table = require("cli-table3");
const config = require("config");
const https = require("https");
const colors = require("colors/safe");

async function authenticate() {
  try {
    console.log(`Trying to login as ${config.login}...`);
    const authParams = { username: config.login, password: config.password };
    const response = await axios.post(`${config.url}/rest/auth/1/session`, authParams);

    // remove empty crowd.token_key setting
    const cookies = response.headers["set-cookie"];
    console.log(`Received ${cookies.length} cookies`);
    return cookies.filter((item) => !item.startsWith('crowd.token_key=""'));
  } catch (error) {
    console.log("Failed to authenticate: ", error);
    return null;
  }
}

async function getItems(cookies) {
  const query = {
    jql: `worklogDate>=startOfMonth() and worklogAuthor=${config.logAuthor}`,
    startAt: 0,
    maxResults: 50,
    fields: ["key", "id"],
  };

  const reqConfig = { headers: { cookie: cookies.join(";") } };
  try {
    const response = await axios.post(`${config.url}/rest/api/2/search`, query, reqConfig);
    if (response.data.total) {
      let items = new Map();
      let names = "";
      response.data.issues.forEach((issue) => {
        items.set(issue.id, issue.key);
        if (names.length !== 0) {
          names += ", ";
        }
        names += issue.key;
      });
      console.log(`Found ${response.data.total} items: ${names}`);
      return items;
    } else {
      console.log("No items found");
      return null;
    }
  } catch (error) {
    console.log("Failed to get items: ", error);
    return null;
  }
}

async function getWorklogs(cookies, items) {
  let reqConfig = { headers: { cookie: cookies.join(";") } };
  if (config.has("tls_v11")) {
    console.log("Using TLSv1.1 for getting worklogs");
    reqConfig.httpsAgent = new https.Agent({
      ciphers: "ALL",
      secureProtocol: "TLSv1_1_method",
    });
  }
  let requests = Array.from(items, ([id, key]) => id).map((id) =>
    axios.get(`${config.url}/rest/api/2/issue/${id}/worklog`, reqConfig)
  );
  console.log("Retrieving worklogs...");
  try {
    const responses = await Promise.all(requests);
    let worklogs = [];
    responses.forEach((response) => Array.prototype.push.apply(worklogs, response.data.worklogs));
    return worklogs;
  } catch (error) {
    console.log("Failed to get worklog", error);
    return null;
  }
}

function timeInSecToHour(value) {
  let hours = value / 3600;
  return Number.isInteger(hours) ? hours.toString() : hours.toFixed(3);
}

function timeInSecToHourColored(value) {
  let hours = value / 3600;
  if (Number.isInteger(hours)) {
    const kHoursPerDay = 8;
    return hours === kHoursPerDay ? colors.green(hours.toString()) : colors.red(hours.toString());
  } else {
    return colors.red(hours.toFixed(3));
  }
}

function worklogFilter(worklog) {
  const authorsMatch = worklog.author.key === worklog.updateAuthor.key;
  const authorIsOrig = worklog.author.key.toUpperCase() === config.logAuthor.toUpperCase();
  const startDate = new Date(worklog.started);
  const match = authorsMatch && authorIsOrig && startDate >= this.startMonth;
  if (!match) {
    this.filtered.push(worklog);
  }
  return match;
}

function parseWorklogs(items, worklogs) {
  const now = new Date();
  let filterContext = { startMonth: new Date(now.getFullYear(), now.getMonth(), 1), filtered: [] };
  worklogs = worklogs.filter(worklogFilter, filterContext);
  console.log(`After filtering: ${worklogs.length} worklogs`);
  console.log("Following worklogs were filtered:", filterContext.filtered);

  let dayLogs = new Map();
  worklogs.forEach((log) => {
    const timezone = log.author.timeZone;
    const date = new Date(log.started).toLocaleDateString("en-US", {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      timeZone: timezone,
    });
    const entry = { key: items.get(log.issueId), time: log.timeSpentSeconds };
    if (dayLogs.has(date)) {
      dayLogs.get(date).push(entry);
    } else {
      dayLogs.set(date, [entry]);
    }
  });
  const sortDateKeys = (a, b) => (a[0] > b[0] ? 1 : -1);
  let dayLogsSorted = new Map([...dayLogs].sort(sortDateKeys));

  console.log(dayLogsSorted);
  let table = new Table({ head: ["Date", "Task", "Time (hours)", "Total"] });
  dayLogsSorted.forEach((subEntries, date, map) => {
    let total = 0;
    subEntries.forEach((entry) => {
      total += entry.time;
    });
    const subEntriesCount = subEntries.length;
    if (subEntriesCount === 1) {
      table.push([
        date,
        subEntries[0].key,
        { hAlign: "center", content: timeInSecToHour(subEntries[0].time) },
        { hAlign: "center", content: timeInSecToHourColored(total) },
      ]);
    } else {
      table.push([
        { rowSpan: subEntriesCount, vAlign: "center", content: date },
        subEntries[0].key,
        { hAlign: "center", content: timeInSecToHour(subEntries[0].time) },
        {
          rowSpan: subEntriesCount,
          vAlign: "center",
          hAlign: "center",
          content: timeInSecToHourColored(total),
        },
      ]);
      for (let i = 1; i < subEntriesCount; i++) {
        table.push([
          subEntries[i].key,
          { hAlign: "center", content: timeInSecToHour(subEntries[i].time) },
        ]);
      }
    }
  });
  console.log(table.toString());
}

(async () => {
  const cookies = await authenticate();
  if (Array.isArray(cookies) && cookies.length) {
    console.log("Authentication passed, retrieving items");

    const items = await getItems(cookies);
    if (items !== null) {
      const worklogs = await getWorklogs(cookies, items);
      if (Array.isArray(worklogs) && worklogs.length) {
        console.log(`Got ${worklogs.length} worklogs`);
        parseWorklogs(items, worklogs);
      } else {
        console.log("No worklogs found");
        process.exit(1);
      }
    } else {
      process.exit(1);
    }
  } else {
    console.log("No cookies found");
    process.exit(1);
  }
})();